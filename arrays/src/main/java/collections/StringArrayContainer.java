package collections;

public class StringArrayContainer {
  private String[] strings = new String[2];
  int lastIndexWithData = 0;

  StringArrayContainer(String first) {
    strings[lastIndexWithData] = first;
  }

  String get(int index) {
    if (index < strings.length && index > 0) {
      return strings[0];
    } else {
      return "THERE IS NO SUCH ELEMENT. BE CAREFUL NEXT TIME!";
    }
  }

  void add(String str) {
    lastIndexWithData++;
    if (lastIndexWithData < strings.length) {
      strings[lastIndexWithData] = str;
    } else {
      String[] temp = strings.clone();
      strings = new String[temp.length * 2];

      for (int i = 0; i < temp.length; i++) {
        strings[i] = temp[i];
      }

      strings[lastIndexWithData] = str;
    }
  }

  void print() {
    for (String string : strings) {
      System.out.println(string + " ");
    }
  }
}
