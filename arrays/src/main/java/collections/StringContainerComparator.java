package collections;

import java.util.Comparator;

public class StringContainerComparator implements Comparator<StringContainer> {
  public int compare(StringContainer that, StringContainer another) {
    return that.getString2().compareTo(another.getString2());
  }
}
