package collections;

public class StringContainer implements Comparable<StringContainer> {
  private String string1;
  private String string2;

   StringContainer(String string1, String string2) {
    this.string1 = string1;
    this.string2 = string2;
  }


  public String getString1() {
    return string1;
  }

  public String getString2() {
    return string2;
  }

  public void setString1(String string1) {
    this.string1 = string1;
  }

  public void setString2(String string2) {
    this.string2 = string2;
  }

  public int compareTo(StringContainer another) {
    return this.getString1().compareTo(another.getString1());
  }

  @Override
  public String toString() {
    return this.getString1() + " " + this.getString2();
  }
}
