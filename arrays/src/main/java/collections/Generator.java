package collections;

import java.util.ArrayList;
import java.util.List;

public class Generator {

   List<StringContainer> generatePairsList() {
    List<StringContainer> pairs = new ArrayList<>();

    pairs.add(new StringContainer("till", "Lindemann"));
    pairs.add(new StringContainer("Gerard", "Way"));
    pairs.add(new StringContainer("Jared", "Leto"));
    pairs.add(new StringContainer("Corey", "Taylor"));
    pairs.add(new StringContainer("Adam", "Gontier"));

    return pairs;
  }

  StringContainer[] generatePairsArray() {
     StringContainer[] pairsArray = new StringContainer[5];

    pairsArray[0] = (new StringContainer("Till", "Lindemann"));
    pairsArray[1] = (new StringContainer("Gerard", "Way"));
    pairsArray[2] = (new StringContainer("Jared", "Leto"));
    pairsArray[3] = (new StringContainer("Corey", "Taylor"));
    pairsArray[4] = (new StringContainer("Adam", "Gontier"));

    return pairsArray;

  }
}
