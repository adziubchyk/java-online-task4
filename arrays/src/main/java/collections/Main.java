package collections;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Main {
  public static void main(String[] args) {
/*    Generator generator = new Generator();

    StringContainer[] pairsArray = generator.generatePairsArray();
    List<StringContainer> pairsList = generator.generatePairsList();

    System.out.println("Unsorted array: ");
    printArray(pairsArray);

    System.out.println("Sorted array: ");
    Arrays.sort(pairsArray);
    printArray(pairsArray);

    System.out.println("Unsorted list: ");
    pairsList.forEach(System.out::println);

    System.out.println("Sorted list: ");
    Collections.sort(pairsList);
    pairsList.forEach(System.out::println);

    System.out.println("\nWith Comparator:\n");
    System.out.println("Sorted array: ");
    Arrays.sort(pairsArray, new StringContainerComparator());
    printArray(pairsArray);

    System.out.println("Sorted list: ");
    Collections.sort(pairsList, new StringContainerComparator());
    pairsList.forEach(System.out::println);

    StringContainer searched = new StringContainer("Till", "Lindemann");

    int found = binarySearch(pairsArray, searched, new StringContainerComparator());
    System.out.println("FOUND: "  + pairsArray[found]);*/

    long startTimeContainer = System.currentTimeMillis();
    StringArrayContainer strings = new StringArrayContainer("shopokatkah");
    for (int i = 0; i < 4999; i++) {
      strings.add(String.valueOf(i));
    }
    //strings.print();
    long endTimeContainer = System.currentTimeMillis();

    System.out.println("With custom container: " + (startTimeContainer - endTimeContainer));

    long startTimeArrayList = System.currentTimeMillis();
    List<String> stringsArrayList = new ArrayList<>();
    for (int i = 0; i < 5000; i++) {
      stringsArrayList.add(String.valueOf(i));
    }
    //stringsArrayList.forEach(System.out::print);
    long endTimeArrayList = System.currentTimeMillis();

    System.out.println("With array list: " + (startTimeArrayList - endTimeArrayList));

  }

  private static int binarySearch(StringContainer[] pairs, StringContainer searched, StringContainerComparator comparator) {
    int left = 0, right = pairs.length - 1, mid = pairs.length / 2, comp;

    while (left < mid) {
      if (comparator.compare(searched, pairs[mid]) > 0) {
        left = mid;
        mid = left + (right - left) / 2;
      } else if (comparator.compare(searched, pairs[mid]) == 0) {
        return mid;
      } else {
        right = mid;
        mid = left + (right - left) / 2;
      }
    }

    return -1;
  }

  private static int binarySearch(int[] pairs, int searched) {
    int left = 0, right = pairs.length - 1, mid = pairs.length / 2;

    while (left < mid) {
      if (searched > pairs[mid]) {
        left = mid;
        mid = left + (right - left) / 2;
      } else if (searched == pairs[mid]) {
        return mid;
      } else {
        right = mid;
        mid = left + (right - left) / 2;
      }
    }

    return -1;
  }

  private static void printArray(StringContainer[] pairs) {
    for (StringContainer pair : pairs) {
      System.out.print(pair + ",");
    }
    System.out.println();
  }
}
