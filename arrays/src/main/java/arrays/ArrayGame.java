package arrays;

import java.util.Random;
import java.util.Scanner;

public class ArrayGame {
  int[] doors;
  private Hero hero;
  private int deadEnd;

  public ArrayGame() {
    doors = new int[10];
    hero = new Hero();
    deadEnd = 0;
  }

  public void fillDoors() {
    System.out.println("How would you like to fill doors?" +
            "\n1) Randomly" + "\n2)Manually");

    int option = readNumberWithBounds(1, 2);

    switch(option) {
      case 0: randomlyFillDoors();
        break;

      case 1: manuallyFillDoors();
        break;
    }

    System.out.println("Doors");
    for(int i = 0; i < doors.length; i++) {
      System.out.println("| Door " + (i + 1) + " | " + doors[i] + " | ");
    }


  }

  private int readNumberWithBounds(int leftBound, int rightBound) {
    int chosenOptionNumber;

    Scanner scanner = new Scanner(System.in, "utf-8");

    do {
      System.out.print("Please enter a valid option: ");

      while (!scanner.hasNextInt()) {
        scanner.next();
        System.out.print("Please enter a valid option: ");
      }

      chosenOptionNumber = scanner.nextInt();
    } while (chosenOptionNumber > rightBound || chosenOptionNumber < leftBound);

    return chosenOptionNumber - 1;
  }

  private void manuallyFillDoors() {

    System.out.print("Negative numbers from -5 to" +
            " - 100 for enemy strength" +
            "\nPositive numbers from 10 to 80");
    boolean isEnemy;

    for (int i = 0; i < doors.length; i++) {
      isEnemy = isEnemyBehindTheDoor();
      if(isEnemy) {
        doors[i] = readNumberWithBounds(-100, -5);
      } else {
        doors[i] = readNumberWithBounds(10, 80);
      }
    }
  }

  private void randomlyFillDoors() {
    Random rand = new Random();
    boolean isEnemy;

    for (int i = 0; i < doors.length; i++) {
      isEnemy = rand.nextBoolean();

      if (isEnemy) {
        doors[i] = -(5 + rand.nextInt(96));
      } else {
        doors[i] = 10 + rand.nextInt(71);
      }
    }
  }

  public int countDeadEnds(int doorNumber) {

    if (doorNumber == doors.length) {
      return 0;
    }

    if(doors[doorNumber] + hero.getStrength() < 0) {
      return (1 + countDeadEnds(++doorNumber));
    } else {
      return countDeadEnds(++doorNumber);
    }
  }

  private boolean isEnemyBehindTheDoor() {
    Scanner sc = new Scanner(System.in, "utf-8");

    while(!sc.hasNextBoolean()) {
      System.out.print("Is enemy behind the next door?" +
              " (true or false)");
      sc.next();
    }

    return  sc.nextBoolean();
  }

  public void getWinningCombination() {
    int[] doorNumbers = new int[doors.length];
    int firstDoorNumber = 0;
    int lastDoorNumber = doorNumbers.length - 1;

    if (canWin()) {

      for (int i = 0; i < doors.length; i++) {
        if (doors[i] + hero.getStrength() > 0) {
          hero.setStrength(hero.getStrength() + doors[i]);
          doorNumbers[firstDoorNumber] = i;
          firstDoorNumber++;
        } else {
          doorNumbers[lastDoorNumber] = i;
          lastDoorNumber--;
        }
      }

      for(int elem : doorNumbers) {
        System.out.print(elem + 1 + " ");
      }
    } else {
      System.out.println("You will be dead anyway!");
    }

  }

  private boolean canWin() {
    boolean canWin;
    int sum = 0;

    for(int elem : doors) {
      sum += elem;
    }

    System.out.println(sum);

    canWin = sum + hero.getStrength() >= 0;

    return canWin;
  }


}
