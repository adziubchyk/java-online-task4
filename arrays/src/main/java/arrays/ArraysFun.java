package arrays;

import java.util.Arrays;

public class ArraysFun {
  public static void main(String[] args) {
    /*int[] array = {50, 1, 23, 47, 23, 47, 92, 92, 92, 47, 47};

    int[] withoutDuplicates = deleteDuplicates(array, false);
    int[] withoutDuplicatesLeavingOne = deleteDuplicates(array, true);

    printArray(withoutDuplicatesLeavingOne);*/

    ArrayGame game = new ArrayGame();
    game.fillDoors();
    game.getWinningCombination();
  }

  private static int[] deleteDuplicates(int[] array, boolean toLeave) {
    Arrays.sort(array);
    printArray(array);

    int[] temp = new int[array.length];
    int j = 0, duplicate = -1;

    for (int i = 0; i < array.length - 2; i++) {
        if (toLeave) {
          if (array[i] != array[i + 1]) {
            temp[j++] = array[i];
          }
        }
        else {

          if (array[i] != array[i + 1]) {
            temp[j++] = array[i];
          } else {
            if (array[i] == array[i + 1] && array[i] == array[i + 2]) {
              duplicate = array[i];

              if (i + 2 < array.length - 1)
                i += 2;
              while (array[i] == array[i + 1]) {
                if (i + 1 < array.length - 1)
                  i++;
                else
                  break;
              }
            }
          }
        }
    }

    if (array[array.length - 1] != duplicate)
      temp[j++] = array[array.length - 1];

    int[] result = new int[j];

    for (int i = 0; i < j; i++) {
      result[i] = temp[i];
    }

    return result;
  }

  private static void printArray(int[] array) {
    for (int elem : array) {
      System.out.print(elem + " ");
    }
    System.out.println("\n");
  }
}
